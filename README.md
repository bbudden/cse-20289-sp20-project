# Project - README

This is the [Final Project] for [CSE 20289 Systems Programming (Spring 2020)].

## Members

- Bradley Budden (bbudden@nd.edu)
- Daraius Balsara (dbalsar2@nd.edu)

## Demonstration

- [Link to Demonstration Video (Youtube)](https://youtu.be/G6TIM4NVpz0)
- [Link to Demonstration Video (Google Drive)](https://drive.google.com/file/d/1vY6p8J5A9v54f2p_LseFjvGzGMpVQ07z/view?usp=sharing)
- [Link to Thumbnail Guru Point Video (Youtube)](https://youtu.be/rDlkVjyMHFk)

## Errata

Summary of things that don't work (quite right).

Currently, all project entities seem to be working correctly. 

## Contributions

Enumeration of the contributions of each group member.

Bradley Budden:
    handler.c 
    request.c
    utils.c
    latency.sh 
    throughput.sh

Daraius Balsara: 
    thor.py
    socket.c
    spidey.c
    single.c
    forking.c
    Makefile
    Video director


[Final Project]: https://www3.nd.edu/~pbui/teaching/cse.20289.sp20/project.html
[CSE 20289 Systems Programming (Spring 2020)]: https://www3.nd.edu/~pbui/teaching/cse.20289.sp20/
