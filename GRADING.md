# Final Project

- Score: 21.75 / 26

## General

- 0.25  Missing ARFLAGS
- 1.00  Valgrind: Invalid read of size 1, segfault

## Thor

## Spidey

- 2.00  Segfaults with CGI
- 1.00  Mimetype fails

## Demonstration

- Nice background music

## Guru Points

+ 1.00  Thumbnail
