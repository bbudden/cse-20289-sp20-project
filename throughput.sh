#!/bin/sh

#globals

URL_BASE="http://student13.cse.nd.edu"
PORT=9898
KB=1024

# Functions

usage() {
    cat 1>&2 <<EOF
Usage: $(basename $0) 

-p    Port

If port is not provided, then it defaults to $PORT.
EOF
    exit $1
}

small_file() {
		# make small static file request
		d1=$(./bin/thor.py -h 1 -t 10 $URL_BASE:$PORT/small_file | grep 'TOTAL'  | grep -Eo '[0-9]?[0-9].[0-9][0-9][0-9]')
				
		formattedVal=`echo "scale=3; $KB / $d1 / $KB" | bc`
		echo $formattedVal Mbytes/sec
}

medium_file() {
		# make medium static file request
		d1=$(./bin/thor.py -h 1 -t 10 $URL_BASE:$PORT/medium_file | grep 'TOTAL'  | grep -Eo '[0-9]?[0-9].[0-9][0-9][0-9]')
				
		formattedVal=`echo "scale=3; 1 / $d1" | bc`
		echo $formattedVal Mbytes/sec
}

large_file() {
		# make medium static file request
		d1=$(./bin/thor.py -h 1 -t 10 $URL_BASE:$PORT/large_file | grep 'TOTAL'  | grep -Eo '[0-9]?[0-9].[0-9][0-9][0-9]')
				
		formattedVal=`echo "scale=3; $KB / $d1" | bc`
		echo $formattedVal Mbytes/sec
}


# Parse Command Line Options

while [ $# -gt 0 ]; do
    case $1 in
        -h) usage 0;;
		-p) shift; PORT=$1;;
    esac
    shift
done

# Display Information

echo "Average Throughput: "
echo "[One hammers, Ten throws for -each- file]"
echo ""
echo "Static File Requests"
echo ""
echo "Small Static File:"
small_file
echo ""
echo "Medium Static File:"
medium_file
echo ""
echo "Large Static File:"
large_file
echo ""


