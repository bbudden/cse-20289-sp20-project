#!/bin/sh

#globals

URL_BASE="http://student13.cse.nd.edu"
PORT=9898

# Functions

usage() {
    cat 1>&2 <<EOF
Usage: $(basename $0) 

-p    Port

If port is not provided, then it defaults to $PORT.
EOF
    exit $1
}

dir_listings() {
		# make directory listing requests for root directory
		echo "/"
		d1=$(./bin/thor.py -h 2 -t 10 $URL_BASE:$PORT | grep 'TOTAL'  | grep -Eo '[0-9]?[0-9].[0-9][0-9][0-9]')
		#echo $d1
		# make directory listing requests for html directory
		echo "/html"
		d2=$(./bin/thor.py -h 2 -t 10 $URL_BASE:$PORT/html | grep 'TOTAL'  | grep -Eo '[0-9]?[0-9].[0-9][0-9][0-9]')
		#echo $d2
		#make directory listing requests for images directory
		echo "/images"
		d3=$(./bin/thor.py -h 2 -t 10 $URL_BASE:$PORT/images | grep 'TOTAL' | grep -Eo '[0-9]?[0-9].[0-9][0-9][0-9]')
		#echo $d3
		# make directory listing requests for scripts directory
		echo "/scripts"
		d4=$(./bin/thor.py -h 2 -t 10 $URL_BASE:$PORT/scripts | grep 'TOTAL'  | grep -Eo '[0-9]?[0-9].[0-9][0-9][0-9]')
		#echo $d4
		# make directory listing requests for text directory
		echo "/text"
		d5=$(./bin/thor.py -h 2 -t 10 $URL_BASE:$PORT/text | grep 'TOTAL'  | grep -Eo '[0-9]?[0-9].[0-9][0-9][0-9]')
		#echo $d5
		# make directory listing requests for text/pass directory
		echo "/text/pass"
		d6=$(./bin/thor.py -h 2 -t 10 $URL_BASE:$PORT/text/pass | grep 'TOTAL'  | grep -Eo '[0-9]?[0-9].[0-9][0-9][0-9]')
		#echo $d6
		
		total=`echo "scale=3; ($d1+$d2+$d3+$d4+$d5+$d6) / 6" | bc`
		echo Directory Listings Average: $total sec
}

static_files() {
		# make static file requests for /song.txt
		echo "/song.txt"
		s1=$(./bin/thor.py -h 2 -t 10 $URL_BASE:$PORT/song.txt | grep 'TOTAL' | grep -Eo '[0-9]?[0-9].[0-9][0-9][0-9]')
		#echo $s1
		#make static file requests for /html/index.html
		echo "/html/index.html"
		s2=$(./bin/thor.py -h 2 -t 10 $URL_BASE:$PORT/html/index.html | grep 'TOTAL' | grep -Eo '[0-9]?[0-9].[0-9][0-9][0-9]')
		#3echo $s2
		# make static file requests for /images/a.png
		echo "/images/a.png"
		s3=$(./bin/thor.py -h 2 -t 10 $URL_BASE:$PORT/images/a.png | grep 'TOTAL' | grep -Eo '[0-9]?[0-9].[0-9][0-9][0-9]')
		#echo $s3
		# make static file requests for /images/b.png
		echo "/images/b.png"
		s4=$(./bin/thor.py -h 2 -t 10 $URL_BASE:$PORT/images/b.png | grep 'TOTAL' | grep -Eo '[0-9]?[0-9].[0-9][0-9][0-9]')
		#echo $s4
		# make static file requests for /images/b.png
		echo "/images/b.png"
		s5=$(./bin/thor.py -h 2 -t 10 $URL_BASE:$PORT/images/c.png | grep 'TOTAL' | grep -Eo '[0-9]?[0-9].[0-9][0-9][0-9]')
		#echo $s5
		# make static file requests for /images/d.png
		echo "/images/d.png"
		s6=$(./bin/thor.py -h 2 -t 10 $URL_BASE:$PORT/images/d.png | grep 'TOTAL' | grep -Eo '[0-9]?[0-9].[0-9][0-9][0-9]')
		#echo $s6
		#make static file requests for /text/hackers.txt
		echo "/text/hackers.txt"
		s7=$(./bin/thor.py -h 2 -t 10 $URL_BASE:$PORT/text/hackers.txt | grep 'TOTAL' | grep -Eo '[0-9]?[0-9].[0-9][0-9][0-9]')
		#echo $s7
		# make static file requests for /text/lyrics.txt
		echo "/text/lyrics.txt"
		s8=$(./bin/thor.py -h 2 -t 10 $URL_BASE:$PORT/text/lyrics | grep 'TOTAL' | grep -Eo '[0-9]?[0-9].[0-9][0-9][0-9]')
		#echo $s8
		# make static file requests for /text/pass/fail
		echo "/text/pass/fail"
		s9=$(./bin/thor.py -h 2 -t 10 $URL_BASE:$PORT/images/a.png | grep 'TOTAL' | grep -Eo '[0-9]?[0-9].[0-9][0-9][0-9]')
		#echo $s9

		total=`echo "scale=3; ($s1+$s2+$s3+$s4+$s5+$s6+$s7+$s8+$s9) / 9" | bc`
		echo Static Files Average: $total sec
}

cgi_scripts() {
		#make cgi request for /scripts/env.sh
		echo "/scripts/env.sh"
		c1=$(./bin/thor.py -h 2 -t 10 $URL_BASE:$PORT/scripts/env.sh | grep 'TOTAL' | grep -Eo '[0-9]?[0-9].[0-9][0-9][0-9]')
		#echo $c1
		#make cgi request for /scripts/hello.py
		echo "/scripts/hello.py"
		c2=$(./bin/thor.py -h 2 -t 10 $URL_BASE:$PORT/scripts/hello.py?user=Bradley_and_Daraius | grep 'TOTAL' | grep -Eo '[0-9]?[0-9].[0-9][0-9][0-9]')
		#echo $c2
		# make cgi request for /scripts/cowsay.sh
		echo "/scripts/cowsay.sh"
		c3=$(./bin/thor.py -h 2 -t 10 $URL_BASE:$PORT/scripts/cowsay.sh?message=hello\&template=default | grep 'TOTAL' | grep -Eo '[0-9]?[0-9].[0-9][0-9][0-9]')
		#echo $c3

		total=`echo "scale=3; ($c1+$c2+$c3) / 3" | bc`
		echo CGI Scripts Average: $total sec
}


# Parse Command Line Options

while [ $# -gt 0 ]; do
    case $1 in
        -h) usage 0;;
		-p) shift; PORT=$1;;
    esac
    shift
done

# Display Information

echo "Average Latency: "
echo "[Two hammers, Ten throws for -each- resource]"
echo""
echo "Directory Listings:"
dir_listings
echo ""
echo "Static Files:"
static_files
echo ""
echo "CGI Scripts:"
cgi_scripts
echo ""


