#!/usr/bin/env python3

import concurrent.futures
import os
import requests
import sys
import time

# Functions

def usage(status=0):
    progname = os.path.basename(sys.argv[0])
    print(f'''Usage: {progname} [-h HAMMERS -t THROWS] URL
    -h  HAMMERS     Number of hammers to utilize (1)
    -t  THROWS      Number of throws per hammer  (1)
    -v              Display verbose output
    ''')
    sys.exit(status)

def hammer(url, throws, verbose, hid):
    ''' Hammer specified url by making multiple throws (ie. HTTP requests).

    - url:      URL to request
    - throws:   How many times to make the request
    - verbose:  Whether or not to display the text of the response
    - hid:      Unique hammer identifier

    Return the average elapsed time of all the throws.
    '''
    
    i = 0                                                                   #i keeps track of # of throws                                                     

    timeList = []                                                           #timeList keeps list of times
   
    while(i < throws):                                                      #while loop to go 'throws' number of times
        
        start = time.time()                                                 #start time stored    
        
        response = requests.get(url)                                        #request made

        end = time.time()                                                   #end time stored

        timeList.append(end-start)                                          #Append the total time requests.get took 

        if(verbose == True):                                                #when user asks for verboseness, print out response text
            data = response.text 
            print(data)
        
        print(f'Hammer: {hid}, Throw: \t {i}, Elapsed Time: {end-start:.2f}')

        i = i + 1                                                           #iterate i
    

    avgTime = sum(timeList)/len(timeList)                                   #avg time is calculated by finding sum of all values in list divided by # of elements in list
    
    print(f'Hammer: {hid}, AVERAGE \t , Elapsed Time: {avgTime:.2f}')
    
    return sum(timeList) / len(timeList)
    
    pass

def do_hammer(args):
    ''' Use args tuple to call `hammer` '''
    return hammer(*args)

    pass

def main():
    hammers = 1
    throws  = 1
    verbose = False

    # Parse command line arguments

    arguments = sys.argv[1:]    

    if len(arguments) == 0:                                                 #When the user passes in nothing, simply usage(1)
        usage(1)

    while len(arguments) and arguments[0].startswith('-'):                  #Parsing arguments with h, t, and v
        arg = arguments.pop(0)
        if arg == '-h':
            hammers = int(arguments.pop(0))

        elif arg == '-t':
            throws = int(arguments.pop(0))

        elif arg == '-v':
            verbose = True

        else:
            usage(1)

    
    if len(arguments) == 1:                                                 #Taking in url if one is given. If one is not given, usage(1)
        URL = arguments.pop(0)
    else:
        usage(1)

    
    # Create pool of workers and perform throws

    hid = 0                                                                 #setting a hammer id that will change based on number of hammers thrown

    args = tuple( (URL, throws, verbose, hid )  for hid in range(hammers))  #Creates tuple of the arguments that need to be passed to executor.map

    with concurrent.futures.ProcessPoolExecutor(hammers) as executor:
        times = executor.map(do_hammer, args)

    totalElapsedTime = 0                                                    #Variable for total elapsed time to find the average
    
    for x in times:
        totalElapsedTime = totalElapsedTime + x                             #For loop that sums all elements in 'times'

    totalAvgElapsedTime = totalElapsedTime / hammers                        #Calculates avg by dividing sum by # of hammers

    print(f'TOTAL AVERAGE ELAPSED TIME: {totalAvgElapsedTime:.2f}')


# Main execution

if __name__ == '__main__':
    main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
