/* handler.c: HTTP Request Handlers */

#include "spidey.h"

#include <errno.h>
#include <limits.h>
#include <string.h>

#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>

/* Internal Declarations */
Status handle_browse_request(Request *request);
Status handle_file_request(Request *request);
Status handle_cgi_request(Request *request);
Status handle_error(Request *request, Status status);

/**
 * Handle HTTP Request.
 *
 * @param   r           HTTP Request structure
 * @return  Status of the HTTP request.
 *
 * This parses a request, determines the request path, determines the request
 * type, and then dispatches to the appropriate handler type.
 *
 * On error, handle_error should be used with an appropriate HTTP status code.
 **/
Status  handle_request(Request *r) {
    
    Status result;

    /* Parse request*/
    if(parse_request(r) == -1){
        result = handle_error(r, HTTP_STATUS_BAD_REQUEST);
        return result;
    }

    /* Determine request path */
    r->path = determine_request_path(r->uri);
    if(!r->path){
        debug("HTTP REQUEST PATH: %s", r->path);
        result = handle_error(r, HTTP_STATUS_NOT_FOUND);
        return result;
    }

    /* Dispatch to appropriate request handler type based on file type */
    struct stat s;
                                                                                    //store file info in stat struct and check for successful stat call
    debug("Stat-ing file");
    if(stat(r->path, &s) == -1){
        debug("Issue with stat call");
        result = handle_error(r, HTTP_STATUS_INTERNAL_SERVER_ERROR);
        return result;
    }

                                                                                        //check for existence of file
    if(access(r->path, F_OK) == -1){
        result = handle_error(r, HTTP_STATUS_NOT_FOUND);
        return result;
    }

    debug("File exists");

                                                                                        //dispatch directory request
    if(S_ISDIR(s.st_mode)){
        debug("File is a directory");
                                                                                        //check if we can read it, call handler if we can
        if(access(r->path, R_OK) != -1)
            result = handle_browse_request(r);
        else{
            result = handle_error(r, HTTP_STATUS_INTERNAL_SERVER_ERROR);
        }
    }

                                                                                        //dispath file or cgi request
    else if(S_ISREG(s.st_mode)){

        if(access(r->path, R_OK) == 0 && access(r->path, X_OK) == -1){
            debug("Files is regular file and non executable");
            result = handle_file_request(r);
        }
                                                                                        //check if we can execute it, call cgi handler if so
        else if(access(r->path, X_OK) == 0){
            debug("File is executable");
            result = handle_cgi_request(r);
        }

        else{
            debug("File exists but there are issues with permissions");
            result = handle_error(r, HTTP_STATUS_INTERNAL_SERVER_ERROR);
        }
    }

    log("HTTP REQUEST STATUS: %s", http_status_string(result));
    
    return result;
}

/**
 * Handle browse request.
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP browse request.
 *
 * This lists the contents of a directory in HTML.
 *
 * If the path cannot be opened or scanned as a directory, then handle error
 * with HTTP_STATUS_NOT_FOUND.
 **/
Status  handle_browse_request(Request *r) {

    debug("I am handling a directory");

    /* Open a directory for reading or scanning */
    struct dirent **entries;
    int n = scandir(r->path, &entries, 0, alphasort);
    if(n < 0){
        debug ("Scandir no good: %s", strerror(errno));
        return handle_error(r, HTTP_STATUS_NOT_FOUND);
    }


    /* Write HTTP Header with OK Status and text/html Content-Type */

    fprintf(r->stream, "HTTP/1.0 200 OK\r\n");
    fprintf(r->stream, "Content-Type: text/html\r\n");
    fprintf(r->stream, "\r\n");


    /* For each entry in directory, emit HTML list item */
    
    //For use in showing thumbnails
    char * pngMimeType = "image/png";
    char * jpgMimeType = "image/jpeg";
    fprintf(r->stream, "<style>img{border: 1px solid #ddd; border-radius: 4x; padding: 5px; width: 150px;}");
    fprintf(r->stream, "img:hover{box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);}</style>");
    

    fprintf(r->stream, "<ul>\n");
    for(int i = 0; i < n; i++){
    
        if(strcmp(entries[i]->d_name, ".") == 0){

            free(entries[i]);
            continue;
        }
        
        if( (r->uri)[strlen(r->uri) - 1] != '/'){
            
            fprintf(r->stream, "<li><a href=\"%s/%s\">%s</a></li>\n", r->uri, entries[i]->d_name, entries[i]->d_name);
            
            //For thumbnails when the directory entry is an image
            if(streq(determine_mimetype(entries[i]->d_name), pngMimeType) || streq(determine_mimetype(entries[i]->d_name), jpgMimeType)){
                fprintf(r->stream, "<a target=\"_blank\" href=\"%s/%s\">", r->uri, entries[i]->d_name);
                fprintf(r->stream, "<img src=\"%s/%s\" height=75 width=75></a>", r->uri, entries[i]->d_name);                
            }
        
        }
        else{
            fprintf(r->stream, "<li><a href=\"%s%s\">%s</a></li>\n", r->uri, entries[i]->d_name, entries[i]->d_name);
            
            //For thumbnails when the directory entry is an image
            if(streq(determine_mimetype(entries[i]->d_name), pngMimeType) || streq(determine_mimetype(entries[i]->d_name), jpgMimeType)){
                fprintf(r->stream, "<a target=\"_blank\" href=\"%s%s\">", r->uri, entries[i]->d_name);
                fprintf(r->stream, "<img src=\"%s%s\" height=75 width=75></a>", r->uri, entries[i]->d_name);                
            }
        }
        
        free(entries[i]);
    }
    fprintf(r->stream, "</ul>\n");

    free(entries);

    /* Return OK */
    return HTTP_STATUS_OK;
}

/**
 * Handle file request.
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP file request.
 *
 * This opens and streams the contents of the specified file to the socket.
 *
 * If the path cannot be opened for reading, then handle error with
 * HTTP_STATUS_NOT_FOUND.
 **/
Status  handle_file_request(Request *r) {
    char buffer[BUFSIZ];
    char *mimetype = NULL;

    /* Open file for reading */ 
    FILE *fs = fopen(r->path, "r");
    if(!fs){
        return HTTP_STATUS_NOT_FOUND;
    }
    
    /* Determine mimetype */
    mimetype = determine_mimetype(r->path);

    /* Write HTTP Headers with OK status and determined Content-Type */
    fprintf(r->stream, "HTTP/1.0 200 OK\r\n");
    fprintf(r->stream, "Content-Type: %s\r\n", mimetype);
    fprintf(r->stream, "\r\n");


    /* Read from file and write to socket in chunks */
    size_t nread = fread(buffer, 1, BUFSIZ, fs);
    
    while(nread > 0){
        
        fwrite(buffer, 1, nread, r->stream);

        nread = fread(buffer, 1, BUFSIZ, fs);

    }

    /* Close file, deallocate mimetype, return OK */
    fclose(fs);
    debug("mimetype: %s", mimetype);
    free(mimetype);
    return HTTP_STATUS_OK;

}

/**
 * Handle CGI request
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP file request.
 *
 * This popens and streams the results of the specified executables to the
 * socket.
 *
 * If the path cannot be popened, then handle error with
 * HTTP_STATUS_INTERNAL_SERVER_ERROR.
 **/
Status  handle_cgi_request(Request *r) {

    char buffer[BUFSIZ];

    /* Export CGI environment variables from request:
     * http://en.wikipedia.org/wiki/Common_Gateway_Interface */

    setenv("DOCUMENT_ROOT", RootPath, 1);
    setenv("QUERY_STRING", r->query, 1);
    setenv("REMOTE_ADDR", r->host, 1);
    setenv("REMOTE_PORT", r->port, 1);
    setenv("REQUEST_METHOD", r->method, 1);
    setenv("REQUEST_URI", r->uri, 1);
    setenv("SCRIPT_FILENAME", r->path, 1);
    setenv("SERVER_PORT", Port, 1); 

    /* Export CGI environment variables from request headers */
    Header * temp = r->headers;
    
    while(temp != NULL){

        if(streq("Host", temp->name))
            setenv("HTTP_HOST", temp->data, 1);
        else if(streq("Accept", temp->name))
            setenv("HTTP_ACCEPT", temp->data, 1);
        else if(streq("Accept-Language", temp->name))
            setenv("HTTP_ACCEPT_LANGUAGE", temp->data, 1);
        else if(streq("Accept-Encoding", temp->name))
            setenv("HTTP_ACCEPT_ENCODING", temp->data, 1);
        else if(streq("Connection", temp->name))
            setenv("HTTP_CONNECTION", temp->data, 1);
        else if(streq("User-Agent", temp->name))
            setenv("HTTP_USER_AGENT", temp->data, 1);
        
        debug("Environmental Variable Added from Headers: Name-%s, Data-%s\n", temp->name, temp->data);

        temp = temp->next;
    }


    /* POpen CGI Script */
    FILE *pfs = popen(r->path, "r");
    
    if(!pfs){
        return HTTP_STATUS_INTERNAL_SERVER_ERROR;
    }

    /* Copy data from popen to socket */

    size_t nread = fread(buffer, 1, BUFSIZ, pfs);
    while(nread > 0){
        fwrite(buffer, 1, nread, r->stream);
        nread = fread(buffer, 1, BUFSIZ, pfs);
    }

    
    /* Close popen, return OK */

    if(pclose(pfs) == -1){
        return HTTP_STATUS_INTERNAL_SERVER_ERROR;
    }

    return HTTP_STATUS_OK;
}

/**
 * Handle displaying error page
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP error request.
 *
 * This writes an HTTP status error code and then generates an HTML message to
 * notify the user of the error.
 **/
Status  handle_error(Request *r, Status status) {
    const char *status_string = http_status_string(status);

    /* Write HTTP Header */   
    fprintf(r->stream, "HTTP/1.0 %s\r\n", status_string);
    fprintf(r->stream, "Content-Type: text/html\r\n");
    fprintf(r->stream, "\r\n");

    /* Write HTML Description of Error*/
    fprintf(r->stream, "<h1>%s<h2>\n", status_string);
    fprintf(r->stream, "<h2>LOL U Thought! But for real, something's zoinked.</h2>\n");
    fprintf(r->stream, "<center><img src=\"https://460nh93yc6hl1sd5uq1553p2-wpengine.netdna-ssl.com/wp-content/uploads/2019/04/mgid_uma_video_vh1-300x169-1.jpg\"></center>\n");
    
    log("handle_error: %s", status_string);

    /* Return specified status */
    return status;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
